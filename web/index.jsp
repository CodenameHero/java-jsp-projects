<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.sql.*"%>
<%@page import="javax.servlet.*"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Online Music Store -- Login</title>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<link rel="icon" type="image/png" href="favicon.png" />
<link href="bootstrap.min.css" rel="stylesheet" />
<link href="material-dashboard.css" rel="stylesheet" />

<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons'
	rel='stylesheet' type='text/css'>
    </head>
    <body>
       <nav class="navbar navbar-primary" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<image src="mw3.jpg" height="50" width="80" class="navbar-brand"></image>
                        <label style="color: #ffffff; font-size:25px;font-family:Jokerman; "><br>ONLINE MUSIC STORE</label>
		</div>
	</div>
	</nav>
    <center>
	<div class="container">
		<div class="col-md-5 pull-left">
			<div class="card card-stats">
				<div class="card-header" data-background-color="purple">
					<i class="material-icons">person</i>
				</div>
				<div class="card-content">
					<h3 class="title">Login into your Account</h3>
					<br>
				</div>
				<div class="card-content">
					<form method="POST" action="">
						<div class="form-group label-floating">
							<label class="control-label">User Id</label> <input type="text"
								class="form-control" name="lg_id">
						</div>
						<div class="form-group label-floating">
							<label class="control-label">Password</label> <input
								type="password" class="form-control" name="lg_pass">
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="LOGIN">
						</div>
                                            
					</form>
				</div>
				<div class="card-footer">
					<div class="stats">
						<i class="material-icons text-danger">help</i> <a
							href="forgotPassword.jsp">Forgot Password?</a>
					</div>
					<div class="stats pull-right">
						<i class="material-icons text-success">add</i> <a
							href="newaccount.jsp">Create new Account</a>
					</div>
				</div>
			</div>
		</div>
            <div>
                <div class="col-md-5 pull-right">
                    <img src="mw2.jpg" height="400" width="350"/>
                </div>
            </div>
    </center>
		
    </body>
    <%
	    Class.forName("org.apache.derby.jdbc.ClientDriver");  
            Connection con=DriverManager.getConnection("jdbc:derby://localhost:1527/OMS","root","qwerty");      
            String query = "select * from userdata where uid=?";
	PreparedStatement ps = con.prepareStatement(query);
	ps.setString(1, request.getParameter("lg_id"));

	if (request.getParameter("lg_id") != null) {
		ResultSet r = ps.executeQuery();
		if (r.next()) {
                    if (r.getString(3).equals(request.getParameter("lg_pass"))){
                response.sendRedirect("http//www.google.com");
                    }
                    else
                    {
                         response.sendRedirect("newaccount.jsp");
                    }}
            }
%>
</html>
